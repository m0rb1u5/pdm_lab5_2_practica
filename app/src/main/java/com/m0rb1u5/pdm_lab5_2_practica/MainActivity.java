package com.m0rb1u5.pdm_lab5_2_practica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
   private Button button;
   private EditText editText, editText2;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      button = (Button) findViewById(R.id.button);
      editText = (EditText) findViewById(R.id.editText);
      editText2 = (EditText) findViewById(R.id.editText2);

      button.setOnLongClickListener(new View.OnLongClickListener() {
         @Override
         public boolean onLongClick(View v) {
            Toast.makeText(v.getContext(), "LongClickListener", Toast.LENGTH_LONG).show();
            return true;
         }
      });

      editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
         @Override
         public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
               Toast.makeText(v.getContext(), "Navegando dentro del EditText", Toast.LENGTH_LONG).show();
            } else {
               Toast.makeText(v.getContext(), "Navegando fuera del EditText", Toast.LENGTH_LONG).show();
            }
         }
      });

      editText.setOnKeyListener(new View.OnKeyListener() {
         @Override
         public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (v.getId()) {
               case R.id.editText:
                  if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_J)) {
                     Toast.makeText(v.getContext(), "Se aplastó la letra J", Toast.LENGTH_LONG).show();
                  }
                  break;
            }
            return false;
         }
      });
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }
}
